#ifndef VM_H
#define VM_H

#include <array>
#include <cstdint>
#include <sstream>

using BYTE = uint8_t;
using WORD = uint16_t;


class VM
{
public:
    enum class AddrMode
    {
        direct,
        indirect
    };

    VM();
    void run();
    std::stringstream &getOutput();
    std::stringstream &getInput();
    std::stringstream &getArguments();

private:
    void fetchOpcode();
    void fetchOperand();
    void eval();
    void setPc(WORD addr);
    BYTE getMem(WORD addr);
    void setMem(WORD addr, BYTE data);
    void callSubrotine(WORD addr);

    BYTE ac = 0;
    WORD pc = 0;
    BYTE opcode = 0;
    WORD operand = 0;
    std::array<BYTE, 0x10000> mem;
    std::stringstream output;
    std::stringstream input;
    std::stringstream arguments;
    std::string inputBuffer = "";
    bool running = false;
    AddrMode addrMode = AddrMode::direct;
};

#endif // VM_H
