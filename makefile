all: sisprog

sisprog: main.cpp vm.cpp assembler.cpp sisprog.cpp
	g++ -O2 -std=c++11 -o sisprog main.cpp vm.cpp assembler.cpp sisprog.cpp
