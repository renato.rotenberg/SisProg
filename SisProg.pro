TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
        vm.cpp \
    assembler.cpp \
    sisprog.cpp

HEADERS += \
        vm.h \
    assembler.h \
    sisprog.h
