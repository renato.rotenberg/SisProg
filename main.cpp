#include <iostream>
#include <sstream>
#include <algorithm>
#include <iomanip>

#include "vm.h"
#include "assembler.h"
#include "sisprog.h"

int main()
{
    SisProg sis;

    sis.start();

    return 0;
}
